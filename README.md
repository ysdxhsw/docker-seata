## Docker部署seate

参考1：https://www.jianshu.com/p/b2c391a393e2

参考2:https://www.gitmemory.com/issue/seata/seata/2959/708191309

**参考2的截图如下：**

要区分这个docker目录和本地目录，要正确填写，不然文件挂载不正确。

![image-20210326000220062](README/image-20210326000220062.png)

### 0、本次的文件目录结构：

![image-20210326001657980](README/image-20210326001657980.png)

![image-20210326001630902](README/image-20210326001630902.png)





### 1、编写docker-compose.yaml文件

**注意：最后编写的docker-compose.yaml文件如下：**

```shell
version: "3"
services:
  seata-server:
    image: seataio/seata-server
    hostname: seata-server
    ports:
      - "8091:8091"
    volumes:
      - /home/docker-seata/seata-config/logs:/root/logs/seata  #日志
    environment:
      - SEATA_PORT=8091
      - STORE_MODE=file
      - SEATA_CONFIG_NAME=file:/seata-server/resources/registry  #docker下目录
    volumes:
      - /home/docker-seata/resources/registry.conf:/seata-server/resources/registry.conf #左边是本地目录
```

#### 注意1：这个文件目录挂载要这样写，不然挂载不上去。

```shell
 volumes:
      - /home/seata/seata-config/resources/registry.conf:/seata-server/resources/registry.conf
```

#### 注意2：registry.conf配置如下：

```shell
registry {
  # file 、nacos 、eureka、redis、zk、consul、etcd3、sofa
  type = "nacos"
  loadBalance = "RandomLoadBalance"
  loadBalanceVirtualNodes = 10

  nacos {
    application = "seata-server"
    serverAddr = "8.140.169.197:8081"
    group = "SEATA_GROUP"
    namespace = "seata"
    cluster = "default"
    username = "nacos"
    password = "nacos"
  }
}

config {
  # file、nacos 、apollo、zk、consul、etcd3
  type = "nacos"
  nacos {
    serverAddr = "8.140.169.197:8081"
    namespace = "seata"
    group = "SEATA_GROUP"
    username = "nacos"
    password = "nacos"
  }
}
```



### 2、执行 .sh 文件，将seata配置推送到nacos的操作命令如下：

```shell
#进入这个文件件
cd /home/docker-seata/nacos-seata/nacos
#执行下面的命令，将配置上传到我部署在阿里云的nacos1上（这里单上传nacos1）
bash nacos-config.sh -h ip地址 -p 8848 -g SEATA_GROUP -t seata -u nacos1 -w nacos1
```

config.txt修改内容如下：

```SHELL
service.vgroupMapping.my_test_tx_group=default
.......
store.db.datasource=druid
store.db.dbType=mysql
store.db.driverClassName=com.mysql.jdbc.Driver
store.db.url=jdbc:mysql://ip地址:3306/seata?useUnicode=true&rewriteBatchedStatements=true
store.db.user=****
store.db.password=****
```

### 3、 seata服务端，建立seata库，运行sql

```sql
-- -------------------------------- The script used when storeMode is 'db' --------------------------------
-- the table to store GlobalSession data
CREATE TABLE IF NOT EXISTS `global_table`
(
    `xid`                       VARCHAR(128) NOT NULL,
    `transaction_id`            BIGINT,
    `status`                    TINYINT      NOT NULL,
    `application_id`            VARCHAR(32),
    `transaction_service_group` VARCHAR(32),
    `transaction_name`          VARCHAR(128),
    `timeout`                   INT,
    `begin_time`                BIGINT,
    `application_data`          VARCHAR(2000),
    `gmt_create`                DATETIME,
    `gmt_modified`              DATETIME,
    PRIMARY KEY (`xid`),
    KEY `idx_gmt_modified_status` (`gmt_modified`, `status`),
    KEY `idx_transaction_id` (`transaction_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- the table to store BranchSession data
CREATE TABLE IF NOT EXISTS `branch_table`
(
    `branch_id`         BIGINT       NOT NULL,
    `xid`               VARCHAR(128) NOT NULL,
    `transaction_id`    BIGINT,
    `resource_group_id` VARCHAR(32),
    `resource_id`       VARCHAR(256),
    `branch_type`       VARCHAR(8),
    `status`            TINYINT,
    `client_id`         VARCHAR(64),
    `application_data`  VARCHAR(2000),
    `gmt_create`        DATETIME(6),
    `gmt_modified`      DATETIME(6),
    PRIMARY KEY (`branch_id`),
    KEY `idx_xid` (`xid`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- the table to store lock data
CREATE TABLE IF NOT EXISTS `lock_table`
(
    `row_key`        VARCHAR(128) NOT NULL,
    `xid`            VARCHAR(128),
    `transaction_id` BIGINT,
    `branch_id`      BIGINT       NOT NULL,
    `resource_id`    VARCHAR(256),
    `table_name`     VARCHAR(32),
    `pk`             VARCHAR(36),
    `gmt_create`     DATETIME,
    `gmt_modified`   DATETIME,
    PRIMARY KEY (`row_key`),
    KEY `idx_branch_id` (`branch_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
```

### 4、执行脚本

```shell
docker-compose up -d
```



### 5、查看日志，说明seata启动成功

```shell
docker logs -f docker-seata_seata-server_1
```

![image-20210326003204670](README/image-20210326003204670.png)



